from hashlib import md5

from flask.ext.sqlalchemy import SQLAlchemy

db = SQLAlchemy()


roles_users = db.Table(
    'roles_users',
    db.Column('user_id', db.Integer(), db.ForeignKey('user.id')),
    db.Column('role_id', db.Integer(), db.ForeignKey('role.id'))
)


class Role(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))


class User(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(300), nullable=False)
    full_name = db.Column(db.String(300), nullable=False)
    active = db.Column(db.Boolean())
    short_bio = db.Column(db.String(50), default="??")

    roles = db.relationship('Role', secondary=roles_users,
                            backref=db.backref('users', lazy='dynamic'))

    @property
    def gravatar_url(self):
        email_hash = md5(self.email.strip().lower()).hexdigest()
        return ("https://www.gravatar.com/avatar/%s?d=identicon&r=PG" %
                email_hash)

    def __repr__(self):
        return "<User id=%s>" % self.id
