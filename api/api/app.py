from flask import Flask, jsonify
from flask.ext.security import (
    RoleMixin,
    SQLAlchemyUserDatastore,
    Security,
    UserMixin,
)


from db import db, User, Role


app = Flask(__name__)
app.config.from_object('api.config')
app.debug = True

db.init_app(app)
db.app = app


class FlaskUser(User, UserMixin):
    pass


class FlaskRole(Role, RoleMixin):
    pass


user_datastore = SQLAlchemyUserDatastore(db, FlaskUser, FlaskRole)
security = Security(app, user_datastore)


@app.route("/")
def home():
    return jsonify({"hello": "world"})
